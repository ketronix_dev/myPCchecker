﻿using System;
using System.Diagnostics;

namespace MyPCcheck
{
    class Program
    {
        static string ExecShellCommand(string command, string arguments = "")
        {
            ProcessStartInfo psi; // объявляем переменную с информацией о процессе.

            if(arguments != "")
            {
                psi = new ProcessStartInfo(command, arguments); // передаем команду процесса.
            }
            else
            {
                psi = new ProcessStartInfo(command); // передаем команду процесса.
            }

            psi.UseShellExecute = false; // переводим программу в CLI режим.
            psi.RedirectStandardOutput = true; // разрешаем читать вывод процесса.

            Process _process = new Process(); // создаем процесс.

            _process.StartInfo = psi; // передаем конфигурацию процесса.
            _process.Start(); // запускаем процесс.

            string output = _process.StandardOutput.ReadToEnd(); // сохраняем вывод в переменную.
            return output;
        }
        static string GetOSName()
        {
            string output = ExecShellCommand("cat", "/etc/os-release");

            string[] lines = output.Split("\n");
            
            string currentDistro = lines[1].Remove(0,6).TrimEnd('"')+ " версии " + lines[2].Remove(0,12).TrimEnd('"');

            return currentDistro;
        }
        static string GetOSUptime()
        {
            string output = ExecShellCommand("uptime");
            string[] time = output.Split(" ");
            
            string currentUptime = time[4].TrimEnd(',');

            return currentUptime;
        }
        static string GetHostCPU()
        {
            string output = ExecShellCommand("lscpu");
            string[] CPU = output.Split("\n");
            
            string currentCPU = CPU[7].Remove(0, 33);

            return currentCPU;
        }
        static string GetHostRAM()
        {
            string output = ExecShellCommand("cat", "/proc/meminfo");
            string[] data = output.Split("\n");
            
            char[] toDel = {' ','k','B', ' '};

            string currentMem = data[0].Trim(toDel).Remove(0, 17);

            float ram = float.Parse(currentMem);

            return ((int)(ram*0.000001f)).ToString();
        }
        static void Main()
        {
            string distro = GetOSName();
            string uptime = GetOSUptime();
            string cpu = GetHostCPU();
            string ram = GetHostRAM();

            Console.WriteLine("Я знаю что ты, черть, сидишь на " + distro ); // выводим вывод в консоль.
            Console.WriteLine("Еще и " + uptime + " не слазишь! Электричество нужно экономить."); // выводим вывод в консоль.
            Console.WriteLine("Твой проц, " + cpu + ", слабоватый, ты шо, денег насобирать на нормальный не смог?"); // выводим вывод в консоль.
            if (int.Parse(ram) > 8)
            {
                Console.WriteLine("Ах ты мажор! Мог бы и подогнать плашечку ОЗУшки кому-нить. Целых " + ram + " гигов!"); // выводим вывод в консоль.
            } 
            else if(int.Parse(ram) < 8)
            {
                Console.WriteLine("Фу бомжара. Всего-то " + ram + " гигов..."); // выводим вывод в консоль.
            }
            else
            {
                Console.WriteLine("Неплохо неплохо, но можно и лучше. В игры с " + ram + " гигами(ом) не поиграешь..."); // выводим вывод в консоль.
            }
        }
    }
}